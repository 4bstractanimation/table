import React from "react";
import {BootstrapTable, TableHeaderColumn,} from 'react-bootstrap-table';
import Draggable from "react-draggable";

class Table extends React.Component {

  constructor() {
    super();
    this.state = {
      data: [
        {id: 0, value1: 432888, value2: 984},
        {id: 1, value1: 43288, value2: 984},
        {id: 2, value1: 432888, value2: 984},
        {id: 3, value1: 432888, value2: 984},
      ],
      width_col1: 100,
      width_col2: 100,

    };
  }

  resizeRow_col1 = (e) => {
    this.setState(p => ({width_col1: p.width_col1 + e.deltaX}))
  };
    resizeRow_col2 = (e) => {
    this.setState(p => ({width_col2: p.width_col2 + e.deltaX}))
  };


  render() {
    return (
        <div className="tableContainer">
          <BootstrapTable
              data={this.state.data}
              containerStyle={{width: "100%"}}
              tableStyle={{}}
              bodyStyle={{}}
              headerStyle={{}}
          >
            <TableHeaderColumn dataField="id" width={this.state.width_col1} isKey>
              Col 1
            </TableHeaderColumn>
            <Draggable
                axis="x"
                defaultClassName="DragHandle"
                defaultClassNameDragging="DragHandleActive"
                onDrag={(event, {deltaX}) =>
                    this.resizeRow_col1({
                      deltaX
                    })
                }
                position={{x: 0}}
                zIndex={999}
            >
              <span className="DragHandleIcon" style={{cursor:"col-resize", position:"absolute", width:30}}>&nbsp;</span>
            </Draggable>
            <TableHeaderColumn dataField="value1"  width={this.state.width_col2}>
              Col 2
            </TableHeaderColumn>
            <Draggable
                axis="x"
                defaultClassName="DragHandle"
                defaultClassNameDragging="DragHandleActive"
                onDrag={(event, {deltaX}) =>
                    this.resizeRow_col2({
                      deltaX
                    })
                }
                position={{x: 0}}
                zIndex={999}
            >
              <span className="DragHandleIcon" style={{cursor:"col-resize",  position:"absolute", width:30}}> &nbsp; </span>
            </Draggable>
            <TableHeaderColumn dataField="value2" width={"100px"}>
              Col 3
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
    );
  }
}


export default Table;